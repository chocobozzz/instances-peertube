import { GlobalStats } from './global-stats.model.js'

export interface GlobalStatsHistory {
  data: {
    date: string, // YYYY-MM-DD
    stats: GlobalStats
  }[]
}
