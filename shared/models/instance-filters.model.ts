import { NSFWPolicyType } from '@peertube/peertube-types'
import { InstanceCustomizations } from './instance-customizations.model.js'

export type InstanceFilters = {
  start: number
  count: number
  sort: string
  signup?: string
  healthy?: string
  nsfwPolicy?: NSFWPolicyType[]
  minUserQuota?: number
  search?: string
  categoryOneOf?: number[]
  languageOneOf?: string[]
  liveEnabled?: string
  customizations?: InstanceCustomizations
  randomSortSeed?: string
  filterTag?: string
}
