import { createRouter, createWebHistory } from 'vue-router'
import InstanceAdd from '@/components/InstanceAdd.vue'
import InstanceList from '@/components/InstanceList.vue'

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect: '/instances'
    },
    {
      path: '/instances',
      name: 'PeerTube instances',
      component: InstanceList
    },
    {
      path: '/instances/add',
      name: 'Add your instance',
      component: InstanceAdd
    },
    {
      path: '/instances/stats',
      name: 'Fediverse stats',
      component: () => import('./components/InstanceStats.vue')
    }
  ]
})
