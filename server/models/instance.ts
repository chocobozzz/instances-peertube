import { About, ActorImage, ServerConfig, ServerStats } from '@peertube/peertube-types'
import { FindAndCountOptions, literal, Op, QueryTypes, WhereOptions } from 'sequelize'
import { AllowNull, Column, CreatedAt, DataType, Default, Is, IsInt, Max, Model, Table, UpdatedAt } from 'sequelize-typescript'
import { Instance, InstanceConnectivityStats, InstanceCustomizations, InstanceFilters, InstanceHost } from '../../shared/models/index.js'
import { isHostValid } from '../helpers/custom-validators/instances.js'
import { logger } from '../helpers/logger.js'
import { INSTANCE_SCORE } from '../initializers/constants.js'
import { getSort, throwIfNotValid } from './utils.js'

@Table({
  tableName: 'instance',
  indexes: [
    {
      fields: [ 'host' ],
      unique: true
    },
    {
      fields: [ 'filterTags' ],
      using: 'gin'
    }
  ]
})
export class InstanceModel extends Model<Partial<InstanceModel>> {

  @AllowNull(false)
  @Is('Host', value => throwIfNotValid(value, isHostValid, 'valid host'))
  @Column
  host: string

  @AllowNull(false)
  @Default(INSTANCE_SCORE.MAX)
  @IsInt
  @Max(INSTANCE_SCORE.MAX)
  @Column
  score: number

  @AllowNull(false)
  @Column(DataType.JSONB)
  stats: ServerStats

  @AllowNull(false)
  @Column(DataType.JSONB)
  connectivityStats: InstanceConnectivityStats

  @AllowNull(false)
  @Column(DataType.JSONB)
  config: ServerConfig

  @AllowNull(false)
  @Column(DataType.JSONB)
  about: About

  @AllowNull(false)
  @Default([])
  @Column(DataType.ARRAY(DataType.INTEGER))
  categories: number[]

  @AllowNull(false)
  @Default([])
  @Column(DataType.ARRAY(DataType.TEXT))
  languages: string[]

  @AllowNull(true)
  @Column
  customizations: InstanceCustomizations

  @AllowNull(false)
  @Default(false)
  @Column
  blacklisted: boolean

  @AllowNull(true)
  @Column(DataType.ARRAY(DataType.STRING))
  filterTags: string[]

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  static loadByHost (host: string) {
    const query = {
      where: {
        host
      }
    }

    return InstanceModel.findOne(query)
  }

  static listForApi (options: InstanceFilters) {
    const query: FindAndCountOptions = {
      offset: options.start,
      limit: options.count,
      order: InstanceModel.getSort(options.sort, options.randomSortSeed),
      where: { [Op.and]: InstanceModel.buildWhereFilters(options) }
    }

    return InstanceModel.findAndCountAll(query)
      .then(({ rows, count }) => {
        return {
          data: rows,
          total: count
        }
      })
  }

  static listForHostsApi (options: InstanceFilters & { since?: string }) {
    const whereAnd = InstanceModel.buildWhereFilters(options)

    if (options.since !== undefined) {
      whereAnd.push({
        createdAt: {
          [Op.gte]: options.since
        }
      })
    }

    const query: FindAndCountOptions = {
      attributes: [ 'host', 'createdAt' ],
      offset: options.start,
      limit: options.count,
      order: InstanceModel.getSort(options.sort, options.randomSortSeed),
      where: { [Op.and]: whereAnd }
    }

    return InstanceModel.findAndCountAll(query)
                        .then(({ rows, count }) => {
                          return {
                            data: rows,
                            total: count
                          }
                        })
  }

  static listLivingWithId () {
    const query = {
      attributes: [ 'id', 'host' ],
      where: {
        score: {
          [Op.gt]: 0
        }
      }
    }

    return InstanceModel.findAll(query)
  }

  static updateInstance (options: {
    id: number
    config: ServerConfig
    stats: ServerStats
    about: About
    connectivityStats: any
  }) {
    const { id, config, stats, about, connectivityStats } = options

    const categories = about?.instance && Array.isArray(about.instance.categories)
      ? about.instance.categories
      : []

    const languages = about?.instance && Array.isArray(about.instance.languages)
      ? about.instance.languages
      : []

    let customizations = InstanceCustomizations.NONE

    if (config?.instance?.name && config.instance.name !== 'PeerTube') {
      customizations |= InstanceCustomizations.NAME
    }

    if (
      config?.instance?.shortDescription &&
      // eslint-disable-next-line max-len
      config.instance.shortDescription !== 'PeerTube, an ActivityPub-federated video streaming platform using P2P directly in your web browser.'
    ) {
      customizations |= InstanceCustomizations.SHORT_DESCRIPTION
    }

    if (about?.instance?.description && about.instance.description !== 'Welcome to this PeerTube instance!') {
      customizations |= InstanceCustomizations.DESCRIPTION
    }

    if (about?.instance?.terms && about.instance.terms !== 'No terms for now.') {
      customizations |= InstanceCustomizations.TERMS
    }

    if (config?.homepage?.enabled) {
      customizations |= InstanceCustomizations.HOMEPAGE
    }

    if (((config?.instance as any)?.banners || []).length !== 0) {
      customizations |= InstanceCustomizations.BANNER
    }

    if (((config?.instance as any)?.avatars || []).length !== 0) {
      customizations |= InstanceCustomizations.AVATAR
    }

    return InstanceModel.update({
      config,
      stats,
      about,
      categories,
      languages,
      connectivityStats,
      customizations
    }, { where: { id } })
  }

  static async removeBadInstances () {
    const instances = await InstanceModel.listBadInstances()

    const instancesRemovePromises = instances.map(instance => instance.destroy())
    await Promise.all(instancesRemovePromises)

    const numberOfInstancesRemoved = instances.length

    if (numberOfInstancesRemoved) logger.info('Removed bad %d instances.', numberOfInstancesRemoved)
  }

  static async updateInstancesScoreAndRemoveBadOnes (goodInstances: number[], badInstances: number[]) {
    if (goodInstances.length === 0 && badInstances.length === 0) return

    logger.info('Updating %d good instances and %d bad instances scores.', goodInstances.length, badInstances.length)

    if (goodInstances.length !== 0) {
      await InstanceModel.incrementScores(goodInstances, INSTANCE_SCORE.BONUS)
        .catch(err => logger.error({ err }, 'Cannot increment scores of good instances.'))
    }

    if (badInstances.length !== 0) {
      await InstanceModel.incrementScores(badInstances, INSTANCE_SCORE.PENALTY)
        .then(() => InstanceModel.removeBadInstances())
        .catch(err => logger.error({ err }, 'Cannot decrement scores of bad instances.'))
    }
  }

  static async getStats (options: {
    includeAll?: boolean
  }) {
    const { includeAll } = options

    const where = includeAll === true
      ? 'WHERE "score" > 0'
      : 'WHERE "blacklisted" IS FALSE'

    const queryStats = 'SELECT ' +
      'COUNT(*) as "totalInstances", ' +
      'COUNT(*) FILTER (WHERE (config -> \'signup\' ->> \'allowed\')::boolean is TRUE) AS "totalInstancesWithAllowedSignup", ' +
      'SUM((stats->>\'totalUsers\')::integer) as "totalUsers", ' +
      'SUM((stats->>\'totalLocalVideos\')::integer) as "totalVideos", ' +
      'SUM((stats->>\'totalLocalVideoComments\')::integer) as "totalVideoComments", ' +
      'SUM((stats->>\'totalLocalVideoViews\')::integer) as "totalVideoViews", ' +
      'SUM((stats->>\'totalLocalVideoFilesSize\')::bigint) as "totalVideoFilesSize", ' +
      'SUM((stats->>\'totalDailyActiveUsers\')::integer) as "totalDailyActiveUsers", ' +
      'SUM((stats->>\'totalWeeklyActiveUsers\')::integer) as "totalWeeklyActiveUsers", ' +
      'SUM((stats->>\'totalMonthlyActiveUsers\')::integer) as "totalMonthlyActiveUsers" ' +
      'FROM "instance" ' +
      `${where}`

    const queryVersions = 'SELECT ' +
      '(config->>\'serverVersion\') as "serverVersion", ' +
      'COUNT(*) as "total" ' +
      'FROM "instance" ' +
      `${where} ` +
      'GROUP BY (config->>\'serverVersion\')'

    const queryPlugins = 'WITH "pluginConfig" AS ' +
      '(SELECT jsonb_array_elements(config->\'plugin\'->\'registered\') AS "json" FROM instance) ' +
      'SELECT COUNT(*) AS "total", "json"->\'name\' AS "name" ' +
      'FROM "pluginConfig" ' +
      'GROUP BY "name"'

    const queryThemes = 'WITH "themeConfig" AS ' +
      '(SELECT jsonb_array_elements(config->\'theme\'->\'registered\') AS "json" FROM instance) ' +
      'SELECT COUNT(*) AS "total", "json"->\'name\' AS "name" ' +
      'FROM "themeConfig" ' +
      'GROUP BY "name"'

    const queryCountries = 'SELECT COUNT(*) as "total", "connectivityStats"->\'country\' AS "country" ' +
      'FROM "instance" ' +
      'WHERE "connectivityStats"->\'country\' IS NOT NULL ' +
      'GROUP BY "country"'

    const promises = [ queryStats, queryVersions, queryPlugins, queryThemes, queryCountries ]
      .map(query => InstanceModel.sequelize.query(query, { type: QueryTypes.SELECT }))

    const [ resStats, resVersions, resPlugins, resThemes, resCountries ] = await Promise.all(promises)

    const firstResStats = resStats[0] as any

    return {
      totalInstances: firstResStats.totalInstances,
      totalUsers: firstResStats.totalUsers,
      totalDailyActiveUsers: firstResStats.totalDailyActiveUsers,
      totalWeeklyActiveUsers: firstResStats.totalWeeklyActiveUsers,
      totalMonthlyActiveUsers: firstResStats.totalMonthlyActiveUsers,
      totalVideos: firstResStats.totalVideos,
      totalVideoComments: firstResStats.totalVideoComments,
      totalVideoViews: firstResStats.totalVideoViews,
      totalVideoFilesSize: firstResStats.totalVideoFilesSize,

      totalInstancesWithAllowedSignup: firstResStats.totalInstancesWithAllowedSignup,

      instanceVersions: resVersions.map((v: any) => ({
        serverVersion: v.serverVersion,
        total: v.total
      })),

      registeredPlugins: resPlugins.map((p: any) => ({
        name: p.name,
        total: p.total
      })),

      registeredThemes: resThemes.map((t: any) => ({
        name: t.name,
        total: t.total
      })),

      instanceCountries: resCountries.map((c: any) => ({
        countryCode: c.country,
        total: c.total
      }))
    }
  }

  private static buildWhereFilters (options: InstanceFilters) {
    const whereAnd: WhereOptions[] = [
      {
        blacklisted: false
      }
    ]

    if (options.healthy !== undefined) {
      const symbol = options.healthy === 'true' ? Op.gte : Op.lt

      whereAnd.push({
        score: {
          [symbol]: INSTANCE_SCORE.HEALTHY_AT
        }
      })
    }

    if (options.signup !== undefined) {
      whereAnd.push({
        config: {
          signup: {
            allowed: options.signup === 'true'
          }
        }
      })
    }

    if (options.nsfwPolicy !== undefined) {
      whereAnd.push({
        config: {
          instance: {
            defaultNSFWPolicy: {
              [Op.any]: options.nsfwPolicy
            }
          }
        }
      })
    }

    if (options.liveEnabled !== undefined) {
      whereAnd.push({
        config: {
          live: {
            enabled: options.liveEnabled === 'true'
          }
        }
      })
    }

    if (options.search) {
      whereAnd.push({
        [Op.or]: [
          {
            host: {
              [Op.iLike]: `%${options.search}%`
            }
          },
          {
            config: {
              instance: {
                name: {
                  [Op.iLike]: `%${options.search}%`
                }
              }
            }
          }
        ]
      })
    }

    if (Array.isArray(options.languageOneOf)) {
      whereAnd.push({
        languages: {
          // FIXME: typings
          [Op.overlap]: options.languageOneOf as any
        }
      })
    }

    if (Array.isArray(options.categoryOneOf)) {
      whereAnd.push({
        categories: {
          // FIXME: typings
          [Op.overlap]: options.categoryOneOf as any
        }
      })
    }

    if (options.minUserQuota) {
      whereAnd.push({
        [Op.or]: [
          {
            config: {
              user: {
                videoQuota: {
                  [Op.gte]: options.minUserQuota
                }
              }
            }
          },
          {
            config: {
              user: {
                videoQuota: {
                  [Op.eq]: -1
                }
              }
            }
          }
        ]
      })
    }

    if (options.customizations) {
      const escapedCustomizations = InstanceModel.sequelize.escape(options.customizations)

      whereAnd.push(literal(`"customizations" & ${escapedCustomizations} = ${escapedCustomizations}`))
    }

    if (options.filterTag) {
      whereAnd.push({
        filterTags: {
          [Op.contains]: [ options.filterTag ]
        }
      })
    }

    return whereAnd
  }

  private static listBadInstances () {
    const query = {
      where: {
        score: {
          [Op.lte]: 0
        },
        blacklisted: false
      },
      logging: false
    }

    return InstanceModel.findAll(query)
  }

  private static incrementScores (instances: number[], value: number) {
    const instancesString = instances.map(id => id.toString()).join(',')

    const query = `UPDATE "instance" SET "score" = LEAST("score" + ${value}, ${INSTANCE_SCORE.MAX}) ` +
      'WHERE id IN (' + instancesString + ')'

    const options = {
      type: QueryTypes.BULKUPDATE
    }

    return InstanceModel.sequelize.query(query, options)
  }

  private static getSort (sort: string, randomSortSeed?: string) {
    const mappingColumns = {
      totalUsers: literal(`stats->'totalUsers'`),
      totalVideos: literal(`stats->'totalVideos'`),
      totalLocalVideos: literal(`stats->'totalLocalVideos'`),
      totalInstanceFollowers: literal(`stats->'totalInstanceFollowers'`),
      totalInstanceFollowing: literal(`stats->'totalInstanceFollowing'`),
      signupAllowed: literal(`config->'signup'->'allowed'`),
      liveEnabled: literal(`COALESCE(config->'live'->'enabled', 'false')`),
      name: literal(`config->'instance'->'name'`),
      version: literal(`config->'serverVersion'`),
      health: 'score',
      country: literal(`"connectivityStats"->'country'`)
    }

    const secondSort = randomSortSeed
      ? [ literal(`md5(id || ${this.sequelize.escape(randomSortSeed)})`), 'ASC' ]
      : [ 'id', 'ASC' ]

    return getSort(sort, secondSort, mappingColumns)
  }

  toFormattedJSON (): Instance {
    return {
      id: this.id,
      host: this.host,

      // config
      name: this.config.instance.name,
      shortDescription: this.config.instance.shortDescription,
      version: this.config.serverVersion,

      signupAllowed: this.config.signup.allowed,
      signupRequiresApproval: this.config.signup.requiresApproval || false,

      userVideoQuota: this.config.user?.videoQuota,

      liveEnabled: this.config.live
        ? this.config.live.enabled
        : false,

      categories: this.categories,
      languages: this.languages,

      autoBlacklistUserVideosEnabled: this.config.autoBlacklist
        ? this.config.autoBlacklist.videos.ofUsers.enabled
        : false,

      defaultNSFWPolicy: this.config.instance.defaultNSFWPolicy,
      isNSFW: this.config.instance.isNSFW,

      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      avatars: this.formatActorImage(this.host, (this.config.instance as any).avatars),
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      banners: this.formatActorImage(this.host, (this.config.instance as any).banners),

      // stats
      totalUsers: this.stats.totalUsers,
      totalVideos: this.stats.totalVideos,
      totalLocalVideos: this.stats.totalLocalVideos,
      totalInstanceFollowers: this.stats.totalInstanceFollowers,
      totalInstanceFollowing: this.stats.totalInstanceFollowing,

      totalModerators: (this.stats as any).totalModerators,
      totalAdmins: (this.stats as any).totalAdmins,

      averageRegistrationRequestResponseTimeMs: (this.stats as any).averageRegistrationRequestResponseTimeMs,
      totalRegistrationRequestsProcessed: (this.stats as any).totalRegistrationRequestsProcessed,
      totalRegistrationRequests: (this.stats as any).totalRegistrationRequests,

      averageAbuseResponseTimeMs: (this.stats as any).averageAbuseResponseTimeMs,
      totalAbusesProcessed: (this.stats as any).totalAbusesProcessed,
      totalAbuses: (this.stats as any).totalAbuses,

      // connectivity
      supportsIPv6: this.connectivityStats ? this.connectivityStats.supportsIPv6 : undefined,
      country: this.connectivityStats ? this.connectivityStats.country : undefined,

      // computed stats
      health: Math.round((this.score / INSTANCE_SCORE.MAX) * 100),

      createdAt: this.createdAt.toISOString()
    }
  }

  private formatActorImage (host: string, actorImages: ActorImage[]) {
    if (!Array.isArray(actorImages)) return []

    return actorImages.map(i => ({
      width: i.width,
      url: `https://${host}${i.path}`
    }))
  }

  toHostFormattedJSON (): InstanceHost {
    return {
      host: this.host
    }
  }
}
