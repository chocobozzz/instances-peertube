import express from 'express'
import { query } from 'express-validator'
import { logger } from '../../helpers/logger.js'
import { areValidationErrors } from './utils.js'

const historyListValidator = [
  query('includeAll')
    .optional()
    .isBoolean()
    .toBoolean().withMessage('Should have a valid includeAll boolean'),

  query('beforeDate')
    .optional()
    .isISO8601().withMessage('Should have a valid beforeDate date'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.query }, 'Checking history list parameters')

    if (areValidationErrors(req, res)) return
    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  historyListValidator
}
