import got from 'got'
import { REQUEST_TIMEOUT } from '../initializers/constants.js'

function doJSONRequest <T> (url: string) {
  return got<T>(url, {
    responseType: 'json',
    timeout: {
      request: REQUEST_TIMEOUT
    }
  })
}

export {
  doJSONRequest
}
