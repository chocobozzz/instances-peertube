import express from 'express'
import { logger } from '../../helpers/logger.js'
import { asyncMiddleware } from '../../middlewares/async.js'
import {
  adminGetInstanceByHostValidator,
  adminSetFilterTagsValidator,
  checkAdminTokenValidator
} from '../../middlewares/validators/admin.js'
import { InstanceModel } from '../../models/instance.js'

const adminRouter = express.Router()

adminRouter.post('/set-filter-tags',
  checkAdminTokenValidator,
  adminSetFilterTagsValidator,
  asyncMiddleware(adminGetInstanceByHostValidator),
  asyncMiddleware(setFilterTags)
)

adminRouter.post('/add-filter-tags',
  checkAdminTokenValidator,
  adminSetFilterTagsValidator,
  asyncMiddleware(adminGetInstanceByHostValidator),
  asyncMiddleware(addFilterTags)
)

adminRouter.post('/remove-filter-tags',
  checkAdminTokenValidator,
  adminSetFilterTagsValidator,
  asyncMiddleware(adminGetInstanceByHostValidator),
  asyncMiddleware(deleteFilterTags)
)

adminRouter.post('/blacklist',
  checkAdminTokenValidator,
  asyncMiddleware(adminGetInstanceByHostValidator),
  asyncMiddleware(blacklist)
)

adminRouter.post('/unblacklist',
  checkAdminTokenValidator,
  asyncMiddleware(adminGetInstanceByHostValidator),
  asyncMiddleware(unblacklist)
)

// ---------------------------------------------------------------------------

export { adminRouter }

// ---------------------------------------------------------------------------

function setFilterTags (req: express.Request, res: express.Response) {
  return internalUpdateFilterTag(req.body.filterTags, res)
}

function addFilterTags (req: express.Request, res: express.Response) {
  const instance: InstanceModel = res.locals.instance

  const filterTagsSet = new Set(instance.filterTags)

  for (const tag of req.body.filterTags) {
    filterTagsSet.add(tag)
  }

  return internalUpdateFilterTag(Array.from(filterTagsSet), res)
}

function deleteFilterTags (req: express.Request, res: express.Response) {
  const instance: InstanceModel = res.locals.instance

  const filterTags = (instance.filterTags || []).filter(t => req.body.filterTags.includes(t) === false)

  return internalUpdateFilterTag(filterTags, res)
}

async function internalUpdateFilterTag (newTags: string[], res: express.Response) {
  const instance: InstanceModel = res.locals.instance

  instance.filterTags = newTags && newTags.length !== 0
    ? newTags
    : null

  logger.info({ filterTags: instance.filterTags }, `Set filter tags to ${instance.host}`)

  await instance.save()

  return res.json({
    host: instance.host,
    filterTags: instance.filterTags
  })
}

// ---------------------------------------------------------------------------

async function blacklist (req: express.Request, res: express.Response) {
  const instance: InstanceModel = res.locals.instance

  logger.info(`Blacklisting ${instance.host}`)

  instance.blacklisted = true
  await instance.save()

  return res.json({
    host: instance.host,
    blacklisted: instance.blacklisted
  })
}

async function unblacklist (req: express.Request, res: express.Response) {
  const instance: InstanceModel = res.locals.instance

  logger.info(`Unblacklisting ${instance.host}`)

  instance.blacklisted = false
  await instance.save()

  return res.json({
    host: instance.host,
    blacklisted: instance.blacklisted
  })
}
