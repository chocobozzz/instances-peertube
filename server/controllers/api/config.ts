import express from 'express'
import { ServerConfig } from '../../../shared/models/server-config.model.js'
import { CONFIG } from '../../initializers/constants.js'

const configRouter = express.Router()

configRouter.get('/config',
  getConfig
)

// ---------------------------------------------------------------------------

export { configRouter }

// ---------------------------------------------------------------------------

function getConfig (req: express.Request, res: express.Response) {
  return res.json({
    instanceClientWarning: CONFIG.INSTANCE.CLIENT_WARNING,
    reportUrl: CONFIG.INSTANCE.REPORT_URL
  } as ServerConfig)
}
